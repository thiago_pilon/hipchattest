package com.hipchat.extract;

import com.hipchat.model.ExtractContent;
import com.hipchat.model.Message;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static junit.framework.Assert.assertEquals;

/**
 * Created by pilon on 6/5/17.
 */
@RunWith(Parameterized.class)
public class MentionExtractorTest {
    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        Object[][] parameters = new Object[][]{
                {"@chris you around?", "[\"chris\"]"},
                {"you around @pilon?", "[\"pilon\"]"},
                {"Hey @andy and @cris, how are you?", "[\"andy\",\"cris\"]"},
                {"Send to this email: t.pilon@gmail.com", "[]"},
                {"Hello there!", "[]"}
        };

        return Arrays.asList(parameters);
    }

    private final String message;
    private final String expectedJSON;

    public MentionExtractorTest(String message, String expectedJSON) {
        this.message = message;
        this.expectedJSON = expectedJSON;
    }

    @Test
    public void shouldConvertMentionMessageToJSON() throws JSONException, InterruptedException {
        MentionExtractor mentionExtractor = new MentionExtractor();

        Observable<ExtractContent> observable = mentionExtractor.extract(Message.createSenderMessage(message));

        TestObserver<ExtractContent> subject = new TestObserver<>();
        observable.subscribe(subject);

        subject.await(2, TimeUnit.MINUTES);

        subject.assertNoErrors();
        subject.assertSubscribed();
        subject.assertComplete();

        List<ExtractContent> values = subject.values();

        assertEquals(expectedJSON, values.get(0).getContent().toString());
    }
}