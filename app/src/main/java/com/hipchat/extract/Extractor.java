package com.hipchat.extract;

import com.hipchat.model.ExtractContent;
import com.hipchat.model.Message;

import io.reactivex.Observable;

/**
 * Created by pilon on 6/5/17.
 */

interface Extractor {
    Observable<ExtractContent> extract(final Message message);
}
