package com.hipchat.model;

/**
 * Created by pilon on 6/5/17.
 */

public class Message {
    private String value;
    private MessageType type;

    private Message(String value, MessageType type) {
        this.value = value;
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public MessageType getType() {
        return type;
    }

    public static Message createSenderMessage(String value) {
        return new Message(value, MessageType.SENDER);
    }

    public static Message createReceiverMessage(String value) {
        return new Message(value, MessageType.RECEIVER);
    }
}