package com.hipchat.extract;

import com.hipchat.model.ExtractContent;
import com.hipchat.model.Message;

import org.json.JSONArray;

import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by pilon on 6/5/17.
 */

class MentionExtractor implements Extractor {

    private Pattern pattern;

    public MentionExtractor() {
        pattern = Pattern.compile("\\B\\@(\\w+)");
    }

    @Override
    public Observable<ExtractContent> extract(final Message message) {

        return Observable.fromCallable(new Callable<ExtractContent>() {
            @Override
            public ExtractContent call() throws Exception {
                JSONArray jsonArray = new JSONArray();

                Matcher matcher = pattern.matcher(message.getValue());

                while (matcher.find()) {
                    jsonArray.put(matcher.group().replace("@", ""));
                }

                return new ExtractContent("mentions", jsonArray);
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.single());
    }
}
