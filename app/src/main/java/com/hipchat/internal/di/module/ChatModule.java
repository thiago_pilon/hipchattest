package com.hipchat.internal.di.module;

import com.hipchat.extract.MessageAnalyser;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pilon on 6/8/17.
 */

@Module
public class ChatModule {
    @Provides
    @Singleton
    MessageAnalyser provideMessageProcessor(){
        return new MessageAnalyser();
    }
}
