package com.hipchat.model;

/**
 * Created by pilon on 6/8/17.
 */

public enum MessageType {
    SENDER,
    RECEIVER
}
