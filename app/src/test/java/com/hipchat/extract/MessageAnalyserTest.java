package com.hipchat.extract;

import com.hipchat.model.Message;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static junit.framework.Assert.assertEquals;

/**
 * Created by pilon on 6/5/17.
 */

@RunWith(Parameterized.class)
public class MessageAnalyserTest {
    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        Object[][] parameters = new Object[][]{
                {"Good morning! (megusta) (coffee) (upvote) @andy!", "{\"emoticons\":[\"megusta\",\"coffee\",\"upvote\"],\"mentions\":[\"andy\"]}"},
                {"Good morning, @andy! (megusta) (coffee) (upvote) http://www.nbcolympics.com","{\"emoticons\":[\"megusta\",\"coffee\",\"upvote\"],\"mentions\":[\"andy\"],\"links\":[{\"title\":\"2018 PyeongChang Olympic Games | NBC Olympics\",\"url\":\"http://www.nbcolympics.com\"}]}"}
        };

        return Arrays.asList(parameters);
    }

    private final String message;
    private final String expectedJSON;

    public MessageAnalyserTest(String message, String expectedJSON) {
        this.message = message;
        this.expectedJSON = expectedJSON;
    }

    @Test
    public void shouldConvertMessageToJSON() throws JSONException, InterruptedException {
        Message message = Message.createSenderMessage(this.message);

        MessageAnalyser messageAnalyser = new MessageAnalyser();
        Observable<JSONObject> observable = messageAnalyser.processMessage(message);

        TestObserver<JSONObject> subject = new TestObserver<>();
        observable.subscribe(subject);

        subject.await(2, TimeUnit.MINUTES);

        subject.assertNoErrors();
        subject.assertSubscribed();
        subject.assertComplete();

        List<JSONObject> values = subject.values();

        assertEquals(expectedJSON, values.get(0).toString());
    }
}
