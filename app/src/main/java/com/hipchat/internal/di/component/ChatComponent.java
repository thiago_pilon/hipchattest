package com.hipchat.internal.di.component;

import com.hipchat.extract.MessageAnalyser;
import com.hipchat.internal.di.module.ChatModule;
import com.hipchat.presentation.activity.ChatActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pilon on 6/8/17.
 */

@Singleton
@Component(modules = {ChatModule.class})
public interface ChatComponent {
    void inject(ChatActivity baseActivity);
}
