package com.hipchat.extract;

import com.hipchat.model.ExtractContent;
import com.hipchat.model.Message;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

/**
 * Created by pilon on 6/5/17.
 */

public final class MessageAnalyser {
    private final List<Extractor> extractors;

    public MessageAnalyser() {
        List<Extractor> list = Arrays.asList(new MentionExtractor(), new EmoticonExtractor(),  new URLExtractor());
        extractors = Collections.unmodifiableList(list);
    }

    public Observable<JSONObject> processMessage(Message message) {
        final List<Observable<ExtractContent>> observables = new ArrayList<>();

        for (Extractor extractor : extractors) {
            Observable<ExtractContent> jsonArrayObservable = extractor.extract(message);

            observables.add(jsonArrayObservable);
        }

        return Observable.zip(observables, new Function<Object[], JSONObject>() {
            @Override
            public JSONObject apply(@NonNull Object[] jsons) throws Exception {
                JSONObject json = new JSONObject();

                for (Object extractContentObject : jsons) {
                    ExtractContent extractContent = (ExtractContent) extractContentObject;
                    if (extractContent.hasContent()) {
                        json.put(extractContent.getKey(), extractContent.getContent());
                    }
                }

                return json;
            }
        });
    }
}
