package com.hipchat.model;

import org.json.JSONArray;

/**
 * Created by pilon on 6/7/17.
 */

public class ExtractContent {
    private String key;
    private JSONArray content;

    public ExtractContent(String key, JSONArray content) {
        this.key = key;
        this.content = content;
    }

    public String getKey() {
        return key;
    }

    public JSONArray getContent() {
        return content;
    }

    public boolean hasContent() {
        return content.length() > 0;
    }
}
