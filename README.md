# README #

### Tests ###

* Unit tests `./gradlew test`
* UI tests `./gradlew connectedCheck`
* All tests `./gradlew test connectedCheck`

### Libs ###
* [RXJava](https://github.com/ReactiveX/RxJava)
* [RXAndroid](https://github.com/ReactiveX/RxAndroid)
* [Butter Knife](http://jakewharton.github.io/butterknife/)
* [Dagger](https://google.github.io/dagger/)
* [jsoup](https://jsoup.org/)