package com.hipchat.extract;

import com.hipchat.model.ExtractContent;
import com.hipchat.model.Message;

import org.json.JSONArray;

import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by pilon on 6/5/17.
 */

class EmoticonExtractor implements Extractor {

    private Pattern pattern;

    public EmoticonExtractor() {
        pattern = Pattern.compile("\\(([^)]+)\\)");
    }

    @Override
    public Observable<ExtractContent> extract(final Message message) {
        return Observable.fromCallable(new Callable<ExtractContent>() {
            @Override
            public ExtractContent call() throws Exception {
                JSONArray jsonArray = new JSONArray();

                Matcher matcher = pattern.matcher(message.getValue());

                while (matcher.find()) {
                    jsonArray.put(matcher.group().replace("(", "").replace(")", ""));
                }

                return new ExtractContent("emoticons", jsonArray);
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.single());
    }
}
