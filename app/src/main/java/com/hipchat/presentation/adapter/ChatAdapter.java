package com.hipchat.presentation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hipchat.R;
import com.hipchat.model.Message;
import com.hipchat.model.MessageType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pilon on 6/7/17.
 */

public class ChatAdapter extends RecyclerView.Adapter<MessageViewHolder> {

    private static final int SENDER_TYPE = 0;
    private static final int RECEIVER_TYPE = 1;

    private List<Message> messages = new ArrayList<>();
    private Context context;

    public ChatAdapter(Context context) {
        this.context = context;
    }

    public void addNewMessage(Message message) {
        messages.add(message);
        notifyItemInserted(messages.size() - 1);
    }

    public List<Message> getMessages() {
        return messages;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == SENDER_TYPE)
            view = LayoutInflater.from(context).inflate(R.layout.cell_sender_message, parent, false);
        else
            view = LayoutInflater.from(context).inflate(R.layout.cell_receiver_message, parent, false);

        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        Message message = messages.get(position);

        holder.getMessageTextView().setText(message.getValue());
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public int getItemViewType(int position) {
        return messages.get(position).getType().equals(MessageType.SENDER) ? SENDER_TYPE : RECEIVER_TYPE;
    }
}
