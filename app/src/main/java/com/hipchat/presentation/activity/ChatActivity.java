package com.hipchat.presentation.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.hipchat.R;
import com.hipchat.extract.MessageAnalyser;
import com.hipchat.internal.di.component.DaggerChatComponent;
import com.hipchat.model.Message;
import com.hipchat.presentation.adapter.ChatAdapter;

import org.json.JSONObject;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ChatActivity extends AppCompatActivity {
    @BindView(R.id.message)
    EditText messageEditText;

    @BindView(R.id.chat)
    RecyclerView chatRecyclerView;

    private ChatAdapter adapter;

    @Inject
    MessageAnalyser messageAnalyser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ButterKnife.bind(this);

        DaggerChatComponent.create().inject(this);

        setupRecycler();
    }

    private void setupRecycler() {
        adapter = new ChatAdapter(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(1);
        layoutManager.setStackFromEnd(true);

        chatRecyclerView.setLayoutManager(layoutManager);

        chatRecyclerView.setAdapter(adapter);
    }

    @OnClick(R.id.send)
    public void sendMessage() {
        if (messageEditText.getText().toString().isEmpty()) {
            return;
        }

        Message message = Message.createSenderMessage(messageEditText.getText().toString());
        adapter.addNewMessage(message);

        processMessage(message);

        messageEditText.setText("");
    }

    private void processMessage(Message message) {
        messageAnalyser.processMessage(message)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<JSONObject>() {
                    @Override
                    public void accept(@NonNull JSONObject jsonObject) throws Exception {
                        adapter.addNewMessage(Message.createReceiverMessage(jsonObject.toString()));
                        scrollToLastMessage();
                    }
                });
    }

    private void scrollToLastMessage() {
        chatRecyclerView.scrollToPosition(adapter.getMessages().size() - 1);
    }
}
