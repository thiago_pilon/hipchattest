package com.hipchat.presentation.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.hipchat.R;

/**
 * Created by pilon on 6/7/17.
 */

class MessageViewHolder extends RecyclerView.ViewHolder {

    private final TextView messageTextView;

    MessageViewHolder(View view) {
        super(view);

        messageTextView = (TextView) view.findViewById(R.id.message);
    }

    TextView getMessageTextView() {
        return messageTextView;
    }
}