package com.hipchat.extract;

import android.util.Log;

import com.hipchat.model.ExtractContent;
import com.hipchat.model.Message;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by pilon on 6/6/17.
 */

public class URLExtractor implements Extractor {

    private Pattern pattern;

    public URLExtractor() {
        pattern = Pattern.compile("https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)");
    }

    @Override
    public Observable<ExtractContent> extract(final Message message) {
        return Observable.fromCallable(new Callable<ExtractContent>() {
            @Override
            public ExtractContent call() throws Exception {
                JSONArray jsonArray = new JSONArray();

                Matcher matcher = pattern.matcher(message.getValue());

                while (matcher.find()) {
                    String url = matcher.group();

                    try {
                        String title = lookupForTitle(url);


                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("title", title);
                        jsonObject.put("url", url);

                        jsonArray.put(jsonObject);
                    } catch (Exception e) {
                        continue;
                    }
                }

                return new ExtractContent("links", jsonArray);
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.single());
    }

    String lookupForTitle(String url) throws IOException {
        String title = "";

        Document doc = Jsoup.connect(url).get();

        Elements titleElement = doc.getElementsByTag("title");
        if (titleElement.hasText()) {
            title = titleElement.text();
        }

        return title;
    }
}
