package com.hipchat.presentation.activity;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.hipchat.R;
import com.hipchat.util.RecyclerViewMatcher;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by pilon on 6/9/17.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ChatActivityTest {

    @Rule
    public ActivityTestRule<ChatActivity> activityRule = new ActivityTestRule<>(ChatActivity.class);

    @Test
    public void shouldShowMentionMessage() {
        onView(ViewMatchers.withId(R.id.message)).perform(typeText("hello @pilon"));

        onView(withId(R.id.send)).perform(click());

        onView(withRecyclerView(R.id.chat).atPosition(0))
                .check(matches(hasDescendant(withText("hello @pilon"))));

        onView(withRecyclerView(R.id.chat).atPosition(1))
                .check(matches(hasDescendant(withText("{\"mentions\":[\"pilon\"]}"))));
    }

    @Test
    public void shouldShowEmotionMessage() {
        onView(withId(R.id.message)).perform(typeText("(coffee)"));

        onView(withId(R.id.send)).perform(click());

        onView(withRecyclerView(R.id.chat).atPosition(0))
                .check(matches(hasDescendant(withText("(coffee)"))));

        onView(withRecyclerView(R.id.chat).atPosition(1))
                .check(matches(hasDescendant(withText("{\"emoticons\":[\"coffee\"]}"))));
    }

    @Test
    public void shouldShowURLMessage() throws InterruptedException {
        onView(withId(R.id.message)).perform(typeText("http://www.nbcolympics.com"));

        onView(withId(R.id.send)).perform(click());

        onView(withRecyclerView(R.id.chat).atPosition(0))
                .check(matches(hasDescendant(withText("http://www.nbcolympics.com"))));

        Thread.sleep(1000);

        onView(withRecyclerView(R.id.chat).atPosition(1))
                .check(matches(hasDescendant(withText("{\"links\":[{\"title\":\"2018 PyeongChang Olympic Games | NBC Olympics\",\"url\":\"http:\\/\\/www.nbcolympics.com\"}]}"))));
    }

    public static RecyclerViewMatcher withRecyclerView(final int recyclerViewId) {
        return new RecyclerViewMatcher(recyclerViewId);
    }
}
