package com.hipchat.extract;

import com.hipchat.model.ExtractContent;
import com.hipchat.model.Message;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static junit.framework.Assert.assertEquals;

/**
 * Created by pilon on 6/6/17.
 */
@RunWith(Parameterized.class)
public class URLExtractorTest {
    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        Object[][] parameters = new Object[][]{
                {"http://www.nbcolympics.com/", "[{\"title\":\"2018 PyeongChang Olympic Games | NBC Olympics\",\"url\":\"http://www.nbcolympics.com/\"}]"},
                {"http://www.google.com/", "[{\"title\":\"Google\",\"url\":\"http://www.google.com/\"}]"},
                {"https://www.youtube.com", "[{\"title\":\"YouTube\",\"url\":\"https://www.youtube.com\"}]"},
                {"https://notrealsite.com", "[]"},
                {"Check this one https://www.youtube.com and this one http://www.google.com", "[{\"title\":\"YouTube\",\"url\":\"https://www.youtube.com\"},{\"title\":\"Google\",\"url\":\"http://www.google.com\"}]"},
                {"Hello there!", "[]"}
        };

        return Arrays.asList(parameters);
    }

    private final String message;
    private final String expectedJSON;

    public URLExtractorTest(String message, String expectedJSON) {
        this.message = message;
        this.expectedJSON = expectedJSON;
    }

    @Test
    public void shouldConvertURLMessageToJSON() throws JSONException, InterruptedException {
        URLExtractor urlExtractor = new URLExtractor();

        Observable<ExtractContent> observable = urlExtractor.extract(Message.createSenderMessage(message));

        TestObserver<ExtractContent> subject = new TestObserver<>();
        observable.subscribe(subject);

        subject.await(2, TimeUnit.MINUTES);

        subject.assertNoErrors();
        subject.assertSubscribed();
        subject.assertComplete();

        List<ExtractContent> values = subject.values();

        assertEquals(expectedJSON, values.get(0).getContent().toString());
    }
}