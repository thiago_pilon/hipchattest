package com.hipchat.extract;

import com.hipchat.model.ExtractContent;
import com.hipchat.model.Message;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static junit.framework.Assert.assertEquals;

/**
 * Created by pilon on 6/5/17.
 */
@RunWith(Parameterized.class)
public class EmoticonExtractorTest {
    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        Object[][] parameters = new Object[][]{
                {"Good morning! (megusta)", "[\"megusta\"]"},
                {"Good morning! (megusta) (coffee) (upvote)", "[\"megusta\",\"coffee\",\"upvote\"]"},
                {"Good morning!", "[]"}
        };

        return Arrays.asList(parameters);
    }

    private final String message;
    private final String expectedJSON;

    public EmoticonExtractorTest(String message, String expectedJSON) {
        this.message = message;
        this.expectedJSON = expectedJSON;
    }

    @Test
    public void shouldConvertMentionMessageToJSON() throws JSONException, InterruptedException {
        EmoticonExtractor mentionExtractor = new EmoticonExtractor();

        Observable<ExtractContent> observable = mentionExtractor.extract(Message.createSenderMessage(message));

        TestObserver<ExtractContent> subject = new TestObserver<>();
        observable.subscribe(subject);

        subject.await(2, TimeUnit.MINUTES);

        subject.assertNoErrors();
        subject.assertSubscribed();
        subject.assertComplete();

        List<ExtractContent> values = subject.values();

        assertEquals(expectedJSON, values.get(0).getContent().toString());
    }
}